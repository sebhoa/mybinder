from ipythonblocks import BlockGrid

BLEU = 52, 152, 219
JAUNE = 247, 220, 111
MAUVE1 = 195, 155, 211
VERT1 = 171, 235, 198
VERT2 = 46, 204, 113
VERT3 = 19, 141, 117
ORANGE = 230, 126, 34
ROUGE = 192, 57, 43
MAUVE2 = 74, 35, 90
GREY = 230, 230, 230 # toujours en dernier

#                   1     2      3       4      5      6       7      8      9
COULEURS = (None, BLEU, JAUNE, MAUVE1, VERT1, VERT2, VERT3, ORANGE, ROUGE, MAUVE2, GREY)

class Tube:
    
    SIZE = 4
    
    def __init__(self, color_ids, tube_id, puzzle):
        self.__id = tube_id
        self.__colors = color_ids
        self.__puzzle = puzzle
    
    @property
    def id(self):
        return self.__id
        
    def __len__(self):
        return len(self.__colors)
        
    def code(self):
        """Associe un entier entre 0 et 9999 au tube (en fonction du contenu)"""
        return sum(c * 10**i for i, c in enumerate(self.__colors))
    
    def colore(self, color_id):
        """demande au puzzle de colorier la grille concernant le dernier emplacement pris du tube"""
        self.__puzzle.colore(self.id, len(self)-1, color_id)  
        
    def est_vide(self):
        return self.__colors == []
    
    def monochrome(self):
        """Renvoie True ssi le tube est vide ou remplit d'une seul liquide"""
        return self.est_vide() or (len(self) == Tube.SIZE and all(self.color(i) == self.color(0) for i in range(Tube.SIZE-1)))
    
    def color(self, cell_id):
        """Renvoie la couleur à la cellule cell_id ou None si vide"""
        return self.__colors[cell_id] if cell_id < len(self) else None
    
    def pop(self):
        """Retire un niveau de liquide et renvoie l'identifiant de la couleur et peint en gris la zone libérée"""
        self.colore(color_id=-1)
        return self.__colors.pop()
    
    def append(self, color):
        """Ajoute un niveaude liquide au tube et procède à la corotaion idoine"""
        self.__colors.append(color)
        self.colore(color_id=self.top())
    
    def top(self):
        """renvoie la couleur du haut du tube"""
        if not self.est_vide():
            return self.__colors[-1]
    
    def verser(self, nb=None):
        """Si nb non précisé on verse tout le liquide possible sinon on verse nb niveaux"""
        if not self.est_vide():
            color = self.pop()
            if nb is None:
                nb = 1
                while self.top() == color:
                    nb += 1
                    self.pop()
            else:
                for _ in range(nb-1):
                    self.pop()
            return nb, color
               
    def accepte(self, nb, color):
        """Le tube peut-il accueillir nb niveaux du liquide de couleur color"""
        return self.est_vide() or Tube.SIZE - len(self) >= nb and self.color(-1) == color
    
    def remplir(self, nb, color):
        """Remplit le tube de nb fois avec le liquide de couleur color"""
        for _ in range(nb):
            self.append(color)

class Puzzle:
    
    def __init__(self, filename):
        self.__tubes = []
        self.__coups = []
        self.__grid = None
        self.__cfgs = []
        self._load(filename)
    
    @property
    def grid(self):
        return self.__grid
    
    @property
    def coups(self):
        return self.__coups
        
    # Méthodes spéciales
    
    def __len__(self):
        return len(self.__tubes)
    
    def __str__(self):
        s = '  '.join([str(e) for e in range(len(self))]) 
        s += '\n'
        for etage in range(Tube.SIZE-1, -1, -1):
            for tube_id in range(len(self)):
                color = self.tube(tube_id).color(etage)
                s += f'{color if color is not None else " "}  '
            s += '\n'
        return s
            
        
    # Méthodes privées
    
    def _load(self, filename):
        with open(filename, 'r', encoding='utf-8') as datas:
            nb_tubes = int(datas.readline().strip())
            self.__grid = BlockGrid(nb_tubes, 4, block_size=20)
            tube_id = 0
            for line in datas:
                self.grid[:,tube_id] = GREY
                color_ids = []
                for cell_id, color_id in enumerate(line.split()):
                    color_id = int(color_id)
                    color_ids.append(color_id)
                    self.colore(tube_id, cell_id, color_id)
                self.__tubes.append(Tube(color_ids, tube_id, self))
                tube_id += 1
        self._add_cfg(self.code())
                                
    def _memorize(self, id1, id2, nb):
        """enregistre le transfère du tube id1 vers le tube id2"""
        self.__coups.append((id1, id2, nb))
    
    def _add_cfg(self, code):
        self.__cfgs.append(code)
    
    def _remove_cfg(self):
        self.__cfgs.pop()
    
    # Méthodes publiques

    def code(self):
        return sorted(tube.code() for tube in self.__tubes)
    
    def colore(self, tube_id, cell_id, color_id):
        self.grid[Tube.SIZE - cell_id - 1, tube_id] = COULEURS[color_id]
    
    def show(self):
        indices = ''
        for e in range(len(self)):
            indices += f'{e}  '
        print(indices)
        self.__grid.show()
    
    def tube(self, tube_id):
        return self.__tubes[tube_id]
    
    def last(self):
        """Renvoie le dernier coup joué"""
        return self.__coups[-1]
    
    def jouer_coups(self, des_coups):
        """Joue tous les coups de l'itérable des_coups"""
        for coup in des_coups:
            self.coup(*coup)
    
    def coup(self, id1, id2):
        """Verse le tube id1 vers le tube id2 et renvoie True si c'est possible"""
        tube1 = self.tube(id1)
        tube2 = self.tube(id2)
        contenu = tube1.verser()
        if contenu is not None:
            nb, color = contenu
            if tube2.accepte(nb, color):
                tube2.remplir(nb, color)
                self._memorize(id1, id2, nb)
                return True
            else:
                tube1.remplir(nb, color)
                return False
        return False
                
    def undo(self):
        """annule le dernier coup"""
        if self.__coups:
            id1, id2, nb = self.__coups.pop()
            tube1 = self.tube(id1)
            tube2 = self.tube(id2)
            _, color = tube2.verser(nb)
            tube1.remplir(nb, color)
    
    def possibles(self):
        """crée la liste des coups possibles"""
        _tmp = []
        for id1 in range(len(self)):
            for id2 in range(len(self)):
                if id2 != id1:
                    if self.coup(id1, id2):
                        _tmp.append((id1, id2))
                        self.undo()
        return _tmp
    
    def win(self):
        """Renvoie True ssi on a regroupé tous les liquides"""
        return all(tube.monochrome() for tube in self.__tubes)
    
    def solve(self):
        """Résolution par backtracking"""
        if self.win():
            return True
        else:
            a_tester = self.possibles()
            for id1, id2 in a_tester:
                self.coup(id1, id2)
                code = self.code()
                if code not in self.__cfgs:
                    self._add_cfg(code.copy())
                    if self.solve():
                        return True
                    else:
                        self._remove_cfg()
                        self.undo()
                else:
                    self.undo()
            return False
